var winMethods = function () {
    this.sethtml = function (data, method) {
        switch (method) {
            case Methods[0]:
                html = "<div class='winlog' style=' width: "
                    + winlogdata["width"] + "px;height: "
                    + winlogdata["height"] + "px;position:fixed;"
                    + "background-color: aquamarine;z-Index:"
                    + winlogdata["zIndex"] + ";"
                    + "background-image:url(" + winlogdata["img"] + ");background-size:100% 100%;'></div>"

                break;
            case Methods[1]:
                html = html.split("><") + "<div class='winlog_x' style='width:15px;height:15px;background-color: 00000055;z-Index:1000;float:right;line-height:15px;text-align:center;'>x</div>"
                break;
        }
    }
};


// 获取当前节点      
function getscript() {
    var script = document.getElementsByTagName("script")
    for (i = 0; i < script.length; i++) {
        if (script[i].getAttribute("src") == "https://js-1300333310.cos.ap-guangzhou.myqcloud.com/winlog/winlog.js") {//  winlog.js
            return script[i];
        }
    }
};

// 获取参数   享元模式
var getdata = function (winlogdata) {
    for (var attribute_name in winlogdata) {
        if (getscript().getAttribute(attribute_name)) {
            if (typeof (winlogdata[attribute_name]) === typeof (0))
                winlogdata[attribute_name] = Number(getscript().getAttribute(attribute_name))
            else
                winlogdata[attribute_name] = getscript().getAttribute(attribute_name)
        }
    };

    return winlogdata;
}



// 模板运动   监察者模式 divrun监察者模式   timer执行

var divrun= function(winlogdata) {
        this.handles = {};
        this.x = winlogdata["left"];
        this.y = winlogdata["top"];
        this.RIGHT = "RIGHT"
        this.LEFT = "LEFT"
        this.TOP = "TOP"
        this.BOTTOM = "BOTTOM"
        this.STOP="STOP"
        this.l_move = winlogdata["moveleft"]<0?"LEFT":"RIGHT";
        this.t_move = winlogdata["movetop"]<0?"TOP":"BOTTOM";
        this.winlog = document.querySelector(".winlog")
        this.winlog_x = document.querySelector(".winlog_x")

    // 订阅事件
    this.on=function(eventType, handle) {
        if (!this.handles.hasOwnProperty(eventType)) {
            this.handles[eventType] = [];
        }
        if (typeof handle == 'function') {
            this.handles[eventType].push(handle);
        } else {
            throw new Error('缺少回调函数');
        }
        return this;
    }

    // 发布事件
    this.emit=function(eventType, ...args) {
        if (this.handles.hasOwnProperty(eventType)) {
            this.handles[eventType].forEach((item, key, arr) => {
                item.apply(null, args);
            })
        } else {
            throw new Error(`"${eventType}"事件未注册`);
        }
        return this;
    }

    // 删除事件
    this.off=function(eventType, handle) {
        if (!this.handles.hasOwnProperty(eventType)) {
            throw new Error(`"${eventType}"事件未注册`);
        } else if (typeof handle != 'function') {
            throw new Error('缺少回调函数');
        } else {
            this.handles[eventType].forEach((item, key, arr) => {
                if (item == handle) {
                    arr.splice(key, 1);
                }
            })
        }
        return this; // 实现链式操作
    }

    this.setl_move = function (l_move) {
        this.l_move = l_move;
    }
    this.sett_move = function (t_move) {
        this.t_move = t_move;
    }

    this.move = function (tomove) {
        // transform: translate(x, y); 
          if(winlogdata.moveleft<0){
            winlogdata.moveleft=-winlogdata.moveleft;
          }
          if(winlogdata.movetop<0){
            winlogdata.movetop=-winlogdata.movetop;
          }

        switch (tomove) {
            case this.RIGHT:  this.x = this.x + winlogdata.moveleft; break;
            case this.LEFT: this.x = this.x - winlogdata.moveleft; break;
            case this.BOTTOM: this.y = this.y + winlogdata.movetop; break;
            case this.TOP: this.y = this.y - winlogdata.movetop; break;
        }
        winlog.style.left = this.x + "px"
        winlog.style.top = this.y + "px"
    }
}

// 窗口监视
var Win = function () {
    // 获取窗口宽度 
    this.getWidth = function () {
        if (window.innerWidth)
            return window.innerWidth;
        else 
        if ((document.body) && (document.body.clientWidth))
            return document.body.clientWidth;
    }

    // 获取窗口高度 
    this.getHeight = function () {
        if (window.innerHeight)
            return window.innerHeight;
        else
         if ((document.body) && (document.body.clientHeight))
            return document.body.clientHeight;
    }
}

var Timer1 = function (r, w) {
    iswhile = true
    this.r = r;
    this.w = w;

    this.start = function () {
        iswhile = true
    }
    this.stop = function () {
        iswhile = false
    }
    getis=function(){
        return iswhile;
    }
    this.clear = function () {
        clearInterval(this.realTimeClData);
    }
    this.realTimeClData = setInterval("setaddr()", 20);
    setaddr = function () {
        var wid=w.getWidth()
        var hei=w.getHeight()
        if (r.x > (wid - winlogdata.width)) {
            r.emit('right');
        } else if (r.x < 0) {
            r.emit('left');
        }
        if (r.y > (hei - winlogdata.height)) {
            r.emit('bottom');
        } else if (r.y < 0) {
            r.emit('top');
        }

        if (getis()==true) {
            r.move(r.l_move)
            r.move(r.t_move)
        }
    }
}








//主要函数 main
//参数定义
var winlogdata = { "top": 0, "left": 0, "width": 0, "movetop": 0, "moveleft": 0, "height": 0, "zIndex": 0, "img": "", "href": ""};
var html = ""
var Methods = ['winlog', 'winlog_x']

winlogdata = getdata(winlogdata)
winmeth = new winMethods();
// 创建模板      桥接模式            //----------prototype在对象创建后才能生效
Methods.forEach((method) => {
    winMethods.prototype[method] = function (data) {
        return this.sethtml(data, method)
    }
})

winmeth.winlog(winlogdata)
winmeth.winlog_x({ "a": "a" })
document.write(html)


var r = new divrun(winlogdata);
var w = new Win();
var rwtime = new Timer1(r, w);

r.on("right", function () {
    r.setl_move(r.LEFT)
})
r.on("left", function () {
    r.setl_move(r.RIGHT)
})
r.on("top", function () {
    r.sett_move(r.BOTTOM)
})
r.on("bottom", function () {
    r.sett_move(r.TOP)
})


var winlog = document.querySelector("."+Methods[0]+"")
var winlog_x = document.querySelector("."+Methods[1]+"")
//控件监听
winlog.onmouseover = function () {
    rwtime.stop();
    winlog.style.cursor = "pointer"
}
winlog.onmouseout = function () {
    rwtime.start();
}
// 跳转
winlog.onclick = function () {
    event.stopPropagation();
    window.location.href = winlogdata["href"];
}
winlog_x.onclick = function () {
    event.stopPropagation();
    rwtime.clear();
    winlog.style.display = "none"
}
