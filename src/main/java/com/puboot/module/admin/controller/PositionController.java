package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.ResultUtil;
import com.puboot.module.admin.model.BizPosition;
import com.puboot.module.admin.service.BizPositionService;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 后台资源位管理
 */
@Controller
@RequestMapping("position")
@AllArgsConstructor
public class PositionController {

    private final BizPositionService positionService;

    @PostMapping("list")
    @ResponseBody
    public PageResultVo loadLinks(BizPosition bizPosition, Integer pageNumber, Integer pageSize) {
        bizPosition.addIgnoreType(6);
        bizPosition.addIgnoreType(7);
        IPage<BizPosition> bizPositionIPage = positionService.pagePositions(bizPosition, pageNumber, pageSize);
        return ResultUtil.table(bizPositionIPage.getRecords(), bizPositionIPage.getTotal());
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("position", new BizPosition().setType(1));
        return CoreConst.ADMIN_PREFIX + "position/form";
    }

    @PostMapping("/add")
    @ResponseBody
    @CacheEvict(value = "position", allEntries = true)
    public ResponseVo add(BizPosition bizPosition) {
        Date date = new Date();
        bizPosition.setCreateTime(date);
        bizPosition.setUpdateTime(date);
        if (bizPosition.getType() == 1) {
            if (bizPosition.getWidth() == null) {
                bizPosition.setWidth(320);
            }
            if (bizPosition.getHeight() == null) {
                bizPosition.setHeight(200);
            }
        }
        boolean i = positionService.save(bizPosition);
        if (i) {
            return ResultUtil.success("新增资源位成功");
        } else {
            return ResultUtil.error("新增资源位失败");
        }
    }

    @GetMapping("/edit")
    public String edit(Model model, Integer id) {
        BizPosition bizPosition = positionService.getById(id);
        model.addAttribute("position", bizPosition);
        return CoreConst.ADMIN_PREFIX + "position/form";
    }

    @PostMapping("/edit")
    @ResponseBody
    @CacheEvict(value = "position", allEntries = true)
    public ResponseVo edit(BizPosition bizPosition) {
        bizPosition.setUpdateTime(new Date());
        if (bizPosition.getType() == 1) {
            if (bizPosition.getWidth() == null) {
                bizPosition.setWidth(320);
            }
            if (bizPosition.getHeight() == null) {
                bizPosition.setHeight(200);
            }
        }
        boolean i = positionService.updateById(bizPosition);
        if (i) {
            return ResultUtil.success("编辑资源位成功");
        } else {
            return ResultUtil.error("编辑资源位失败");
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = positionService.deleteBatch(ids);
        if (i > 0) {
            return ResultUtil.success("删除资源位成功");
        } else {
            return ResultUtil.error("删除资源位失败");
        }
    }

}
