package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.ResultUtil;
import com.puboot.module.admin.model.BizPosition;
import com.puboot.module.admin.service.BizPositionService;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 专题专栏管理
 */
@Controller
@RequestMapping("subject")
@AllArgsConstructor
public class SubjectController {

    private final BizPositionService positionService;

    @PostMapping("list")
    @ResponseBody
    public PageResultVo loadLinks(BizPosition bizPosition, Integer pageNumber, Integer pageSize) {
        bizPosition.setType(6);
        IPage<BizPosition> bizPositionIPage = positionService.pagePositions(bizPosition, pageNumber, pageSize);
        return ResultUtil.table(bizPositionIPage.getRecords(), bizPositionIPage.getTotal());
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("subject", new BizPosition().setType(6));
        return CoreConst.ADMIN_PREFIX + "subject/form";
    }

    @PostMapping("/add")
    @ResponseBody
    @CacheEvict(value = "subject", allEntries = true)
    public ResponseVo add(BizPosition bizPosition) {
        Date date = new Date();
        bizPosition.setCreateTime(date);
        bizPosition.setUpdateTime(date);
        bizPosition.setType(6);
        boolean i = positionService.save(bizPosition);
        if (i) {
            return ResultUtil.success("新增专题专栏成功");
        } else {
            return ResultUtil.error("新增专题专栏失败");
        }
    }

    @GetMapping("/edit")
    public String edit(Model model, Integer id) {
        BizPosition bizPosition = positionService.getById(id);
        model.addAttribute("subject", bizPosition);
        return CoreConst.ADMIN_PREFIX + "subject/form";
    }

    @PostMapping("/edit")
    @ResponseBody
    @CacheEvict(value = "subject", allEntries = true)
    public ResponseVo edit(BizPosition bizPosition) {
        bizPosition.setUpdateTime(new Date());
        boolean i = positionService.updateById(bizPosition);
        if (i) {
            return ResultUtil.success("编辑专题专栏成功");
        } else {
            return ResultUtil.error("编辑专题专栏失败");
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = positionService.deleteBatch(ids);
        if (i > 0) {
            return ResultUtil.success("删除专题专栏成功");
        } else {
            return ResultUtil.error("删除专题专栏失败");
        }
    }

}
