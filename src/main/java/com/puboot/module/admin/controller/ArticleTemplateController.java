package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.Pagination;
import com.puboot.common.util.ResultUtil;
import com.puboot.module.admin.model.BizArticle;
import com.puboot.module.admin.model.BizArticleTemplate;
import com.puboot.module.admin.service.BizArticleTemplateService;
import com.puboot.module.admin.vo.ArticleTemplateConditionVo;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("articleTemplate")
@AllArgsConstructor
@Slf4j
public class ArticleTemplateController {
    private final BizArticleTemplateService templateService;

    @PostMapping("list")
    @ResponseBody
    public PageResultVo loadArticle(ArticleTemplateConditionVo articleTemplateConditionVo, Integer pageNumber, Integer pageSize) {
        IPage<BizArticleTemplate> page = new Pagination<>(pageNumber, pageSize);
        List<BizArticleTemplate> templateList = templateService.findByCondition(page, articleTemplateConditionVo);
        return ResultUtil.table(templateList, page.getTotal());
    }

    @GetMapping("/add")
    public String addArticle(Model model) {
        BizArticleTemplate bizArticleTemplate = new BizArticleTemplate().setTempType(0);
        model.addAttribute("articleTemplate", bizArticleTemplate);
        return CoreConst.ADMIN_PREFIX + "articleTemplate/publish";
    }

    @PostMapping("/add")
    @ResponseBody
    @Transactional
    @CacheEvict(value = "articleTemplate", allEntries = true)
    public ResponseVo add(BizArticleTemplate template) {
        try {
            templateService.insertArticleTemplate(template);
            return ResultUtil.success("保存模板成功");
        } catch (Exception e) {
            return ResultUtil.error("保存模板失败");
        }
    }

    @GetMapping("/edit")
    public String edit(Model model, Integer id) {
        BizArticleTemplate template = templateService.selectById(id);
        model.addAttribute("articleTemplate", template);
        return CoreConst.ADMIN_PREFIX + "articleTemplate/publish";
    }

    @PostMapping("/edit")
    @ResponseBody
    @CacheEvict(value = "articleTemplate", allEntries = true)
    public ResponseVo edit(BizArticleTemplate template) {
        templateService.updateById(template);
        return ResultUtil.success("编辑模板成功");
    }

    @PostMapping("/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = templateService.deleteBatch(ids);
        if (i > 0) {
            return ResultUtil.success("删除文章成功");
        } else {
            return ResultUtil.error("删除文章失败");
        }
    }
}
