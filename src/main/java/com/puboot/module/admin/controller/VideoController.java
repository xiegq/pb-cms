package com.puboot.module.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.ResultUtil;
import com.puboot.module.admin.model.BizPosition;
import com.puboot.module.admin.service.BizPositionService;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 视频专区管理
 */
@Controller
@RequestMapping("video")
@AllArgsConstructor
public class VideoController {

    private final BizPositionService positionService;

    @PostMapping("list")
    @ResponseBody
    public PageResultVo loadLinks(BizPosition bizPosition, Integer pageNumber, Integer pageSize) {
        bizPosition.setType(7);
        IPage<BizPosition> bizPositionIPage = positionService.pagePositions(bizPosition, pageNumber, pageSize);
        return ResultUtil.table(bizPositionIPage.getRecords(), bizPositionIPage.getTotal());
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("video", new BizPosition().setType(7));
        return CoreConst.ADMIN_PREFIX + "video/form";
    }

    @PostMapping("/add")
    @ResponseBody
    @CacheEvict(value = "video", allEntries = true)
    public ResponseVo add(BizPosition bizPosition) {
        Date date = new Date();
        bizPosition.setCreateTime(date);
        bizPosition.setUpdateTime(date);
        bizPosition.setType(7);
        bizPosition.setUrl("#");
        boolean i = positionService.save(bizPosition);
        if (i) {
            return ResultUtil.success("新增视频专区成功");
        } else {
            return ResultUtil.error("新增视频专区失败");
        }
    }

    @GetMapping("/edit")
    public String edit(Model model, Integer id) {
        BizPosition bizPosition = positionService.getById(id);
        model.addAttribute("video", bizPosition);
        return CoreConst.ADMIN_PREFIX + "video/form";
    }

    @PostMapping("/edit")
    @ResponseBody
    @CacheEvict(value = "video", allEntries = true)
    public ResponseVo edit(BizPosition bizPosition) {
        bizPosition.setUpdateTime(new Date());
        boolean i = positionService.updateById(bizPosition);
        if (i) {
            return ResultUtil.success("编辑视频专区成功");
        } else {
            return ResultUtil.error("编辑视频专区失败");
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        return deleteBatch(new Integer[]{id});
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = positionService.deleteBatch(ids);
        if (i > 0) {
            return ResultUtil.success("删除视频专区成功");
        } else {
            return ResultUtil.error("删除视频专区失败");
        }
    }

}
