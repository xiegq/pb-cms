package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.module.admin.mapper.BizArticleTemplateMapper;
import com.puboot.module.admin.model.BizArticleTemplate;
import com.puboot.module.admin.service.BizArticleTemplateService;
import com.puboot.module.admin.vo.ArticleTemplateConditionVo;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class BizArticleTemplateServiceImpl extends ServiceImpl<BizArticleTemplateMapper, BizArticleTemplate> implements BizArticleTemplateService {

    private final BizArticleTemplateMapper bizArticleTemplateMapper;

    @Override
    public List<BizArticleTemplate> findByCondition(IPage<BizArticleTemplate> page, ArticleTemplateConditionVo vo) {
        return bizArticleTemplateMapper.findByCondition(page, vo);
    }

    @Override
    @Cacheable(value = "articleTemplate", key = "#id")
    public BizArticleTemplate selectById(Integer id) {
        return bizArticleTemplateMapper.getById(id);
    }

    @Override
    @CacheEvict(value = "articleTemplate", allEntries = true)
    public BizArticleTemplate insertArticleTemplate(BizArticleTemplate bizArticleTemplate) {
        Date date = new Date();
        bizArticleTemplate.setCreateTime(date);
        bizArticleTemplate.setUpdateTime(date);
        bizArticleTemplateMapper.insert(bizArticleTemplate);
        return bizArticleTemplate;
    }

    @Override
    @CacheEvict(value = "articleTemplate", allEntries = true)
    public int deleteBatch(Integer[] ids) {
        return bizArticleTemplateMapper.deleteBatch(ids);
    }
}
