package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.common.util.Pagination;
import com.puboot.module.admin.mapper.BizPositionMapper;
import com.puboot.module.admin.model.BizPosition;
import com.puboot.module.admin.service.BizPositionService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BizPositionServiceImpl extends ServiceImpl<BizPositionMapper, BizPosition> implements BizPositionService {

    private final BizPositionMapper positionMapper;

    @Override
    @Cacheable(value = "position", key = "'list'")
    public List<BizPosition> selectPositions(BizPosition bizPosition) {
        return positionMapper.selectPositions(null, bizPosition);
    }

    @Override
    public IPage<BizPosition> pagePositions(BizPosition bizPosition, Integer pageNumber, Integer pageSize) {
        IPage<BizPosition> page = new Pagination<>(pageNumber, pageSize);
        page.setRecords(positionMapper.selectPositions(page, bizPosition));
        return page;
    }

    @Override
    @CacheEvict(value = "position", allEntries = true)
    public int deleteBatch(Integer[] ids) {
        return positionMapper.deleteBatch(ids);
    }

}
