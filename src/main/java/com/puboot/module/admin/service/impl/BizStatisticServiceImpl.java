package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.puboot.module.admin.model.BizArticleLook;
import com.puboot.module.admin.service.*;
import com.puboot.module.admin.vo.StatisticVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Nobita
 * @date 2021/1/12 3:35 下午
 */
@Service
@AllArgsConstructor
public class BizStatisticServiceImpl implements BizStatisticService {

    private final BizArticleService articleService;
    private final BizCommentService commentService;
    private final BizArticleLookService articleLookService;
    private final BizCategoryService categoryService;

    @Override
    public StatisticVo indexStatistic() {
        long articleCount = articleService.count();
//        long commentCount = commentService.count();
        long categoryCount = categoryService.count();
        long lookCount = articleLookService.count();
        long userCount = articleLookService.count(Wrappers.<BizArticleLook>query().select("DISTINCT user_ip"));
        int recentDays = 6;
        Map<String, Integer> lookCountByDay = articleLookService.lookCountByDay(recentDays);
        Map<String, Integer> userCountByDay = articleLookService.userCountByDay(recentDays);
        return StatisticVo.builder().categoryCount(categoryCount).articleCount(articleCount).commentCount(0L).lookCount(lookCount).userCount(userCount).lookCountByDay(lookCountByDay).userCountByDay(userCountByDay).build();
    }
}
