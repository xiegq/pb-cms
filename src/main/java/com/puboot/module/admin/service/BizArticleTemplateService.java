package com.puboot.module.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.module.admin.model.BizArticleTemplate;
import com.puboot.module.admin.vo.ArticleTemplateConditionVo;

import java.util.List;

public interface BizArticleTemplateService extends IService<BizArticleTemplate> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    List<BizArticleTemplate> findByCondition(IPage<BizArticleTemplate> page, ArticleTemplateConditionVo vo);

    /**
     * 根据id获取
     *
     * @param id
     * @return
     */
    BizArticleTemplate selectById(Integer id);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    int deleteBatch(Integer[] ids);

    BizArticleTemplate insertArticleTemplate(BizArticleTemplate bizArticleTemplate);
}
