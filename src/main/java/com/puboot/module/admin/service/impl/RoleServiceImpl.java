package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.common.util.Pagination;
import com.puboot.common.util.UUIDUtil;
import com.puboot.module.admin.mapper.*;
import com.puboot.module.admin.model.*;
import com.puboot.module.admin.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Service
@AllArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    private final RoleMapper roleMapper;
    private final PermissionMapper permissionMapper;
    private final RolePermissionMapper rolePermissionMapper;
    private final UserMapper userMapper;
    private final RoleCategoryMapper roleCategoryMapper;

    @Override
    public Set<String> findRoleByUserId(String userId) {
        return roleMapper.findRoleByUserId(userId);
    }

    @Override
    public IPage<Role> selectRoles(Role role, Integer pageNumber, Integer pageSize) {
        IPage<Role> page = new Pagination<>(pageNumber, pageSize);
        return roleMapper.selectRoles(page, role);
    }

    @Override
    public int insert(Role role) {
        role.setRoleId(UUIDUtil.getUniqueIdByUUId());
        role.setStatus(1);
        role.setCreateTime(new Date());
        return roleMapper.insert(role);
    }

    @Override
    public int updateStatusBatch(List<String> roleIds, Integer status) {
        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("roleIds", roleIds);
        params.put("status", status);
        return roleMapper.updateStatusBatch(params);
    }

    @Override
    public Role findById(String roleId) {
        return roleMapper.selectOne(Wrappers.<Role>lambdaQuery().eq(Role::getRoleId, roleId));
    }

    @Override
    public int updateByRoleId(Role role) {
        Map<String, Object> params = new HashMap<>(3);
        params.put("name", role.getName());
        params.put("description", role.getDescription());
        params.put("role_id", role.getRoleId());
        return roleMapper.updateByRoleId(params);
    }

    @Override
    public List<Permission> findPermissionsByRoleId(String roleId) {
        return permissionMapper.findByRoleId(roleId);
    }

    @Override
    public void addAssignPermission(String roleId, List<String> permissionIds) {
        rolePermissionMapper.delete(Wrappers.<RolePermission>lambdaQuery().eq(RolePermission::getRoleId, roleId));
        for (String permissionId : permissionIds) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(permissionId);
            rolePermissionMapper.insert(rolePermission);
        }
    }

    @Override
    public List<User> findByRoleId(String roleId) {
        return userMapper.findByRoleId(roleId);
    }

    @Override
    public List<User> findByRoleIds(List<String> roleIds) {
        return userMapper.findByRoleIds(roleIds);
    }

    @Override
    public Set<Integer> findCategoryByRoleId(String roleId) {
        return roleMapper.findCategoryByRoleId(roleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAssignCategory(String roleId, List<String> categoryIds) {
        roleCategoryMapper.delete(Wrappers.<RoleCategory>lambdaQuery().eq(RoleCategory::getRoleId, roleId));
        for (String categoryId : categoryIds) {
            RoleCategory roleCategory = new RoleCategory();
            roleCategory.setCategoryId(categoryId);
            roleCategory.setRoleId(roleId);
            roleCategoryMapper.insert(roleCategory);
        }
    }

    @Override
    public List<Integer> findCategoryIdsByUser(String userId) {
        return roleCategoryMapper.findCategoryIdsByUser(userId);
    }

}
