package com.puboot.module.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.module.admin.model.BizPosition;

import java.util.List;

public interface BizPositionService extends IService<BizPosition> {

    List<BizPosition> selectPositions(BizPosition bizPosition);

    IPage<BizPosition> pagePositions(BizPosition bizPosition, Integer pageNumber, Integer pageSize);

    int deleteBatch(Integer[] ids);

}
