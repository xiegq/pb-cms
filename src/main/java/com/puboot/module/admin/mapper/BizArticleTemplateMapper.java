package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.module.admin.model.BizArticleTemplate;
import com.puboot.module.admin.vo.ArticleTemplateConditionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BizArticleTemplateMapper extends BaseMapper<BizArticleTemplate> {

    List<BizArticleTemplate> findByCondition(@Param("page") IPage<BizArticleTemplate> page, @Param("vo") ArticleTemplateConditionVo vo);

    BizArticleTemplate getById(Integer id);

    int deleteBatch(Integer[] ids);
}
