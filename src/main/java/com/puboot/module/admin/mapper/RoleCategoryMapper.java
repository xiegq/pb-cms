package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.RoleCategory;

import java.util.List;

public interface RoleCategoryMapper extends BaseMapper<RoleCategory> {
    List<Integer> findCategoryIdsByUser(String userId);
}