package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.module.admin.model.BizPosition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BizPositionMapper extends BaseMapper<BizPosition> {

    List<BizPosition> selectPositions(@Param("page") IPage<BizPosition> page, @Param("bizPosition") BizPosition bizPosition);

    int deleteBatch(Integer[] ids);

}
