package com.puboot.module.admin.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.puboot.module.admin.vo.base.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class BizPosition extends BaseVo {
    private static final long serialVersionUID = -6511423333796987523L;
    private Integer type; // 1=首页飘窗 2=首页右上 3=首页右下 4=首页左上 5=首页左下 6-专题专栏 7-视频专区
    private String url;
    private String img;
    private String title; // 视频专区 标题
    private String comment; // 视频专区 简介
    private Integer showOrder; //视频专区 顺序
    private Integer width;
    private Integer height;
    @TableField(exist = false)
    private List<Integer> ignoreTypeList;
    @TableField(exist = false)
    private Boolean showOrderSort = false;// sql是否排序

    public void addIgnoreType(Integer type) {
        if (ignoreTypeList == null) {
            ignoreTypeList = new ArrayList<>();
        }
        ignoreTypeList.add(type);
    }
}
