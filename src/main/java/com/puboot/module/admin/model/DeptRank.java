package com.puboot.module.admin.model;

import lombok.Data;

@Data
public class DeptRank {
    private static final long serialVersionUID = 6825108677279625435L;
    private String deptName;
    private Integer articleCount;
}
