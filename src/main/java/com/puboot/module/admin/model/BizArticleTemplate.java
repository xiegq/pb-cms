package com.puboot.module.admin.model;

import com.puboot.module.admin.vo.base.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class BizArticleTemplate extends BaseVo {
    private static final long serialVersionUID = 7238198006412853246L;

    private String title;
    private Integer tempType;
    private String content;
}
