package com.puboot.module.admin.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleCategory implements Serializable {

    private static final long serialVersionUID = 2455220013366482854L;

    private Integer id;

    /**
     * 分类id
     */
    private String categoryId;

    /**
     * 角色id
     */
    private String roleId;

}